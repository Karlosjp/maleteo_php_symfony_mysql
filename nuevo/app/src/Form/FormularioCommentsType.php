<?php

namespace App\Form;

use App\Entity\Comments;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormularioCommentsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null, [
                'attr' =>[
                    'placeholder' => 'nombre'
                ]
            ])
            ->add('localizacion', ChoiceType::class, [
                'attr' => [
                    'placeholder' => 'Localizacion'
                ],'choices' => [
                    'Barcelona' => 'Barcelona',
                    'Sevilla' => 'Sevilla',
                    'Cueuta' => 'Cueuta',
                    'Malaga' => 'Malaga',
                    'Badajoz' => 'Badajoz' 
                ]
            ])
            ->add('comentario', TextareaType::class, [
                'attr' =>[
                    'placeholder' => 'Comenta'
                ]
            ])
            ->add('email', EmailType::class, [
                'attr' =>[
                    'placeholder' => 'email'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comments::class,
        ]);
    }
}
